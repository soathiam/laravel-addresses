# Laravel Addresses

[![Total Downloads](https://poser.pugx.org/soatdev/addresses/downloads)](https://packagist.org/packages/soatdev/addresses)
[![Latest Stable Version](https://poser.pugx.org/soatdev/addresses/v/stable)](https://packagist.org/packages/soatdev/addresses)
[![Latest Unstable Version](https://poser.pugx.org/soatdev/addresses/v/unstable)](https://packagist.org/packages/soatdev/addresses)
[![License](https://poser.pugx.org/soatdev/addresses/license)](https://packagist.org/packages/soatdev/addresses)

Laravel addresses is a bundle for Laravel, providing address management and geolocation capabilities to any classes by using custom database tables and traits. It can be used to add one or multiple addresses to any class using its trait and comes with prebuilt form components to let users manage addresses.

This is my fisrt package and it still needs a lot of work but it is a start.

--
**Please note that this bundle is Laravel 5 only, it has not been tested on older versions**

## Installation

Add `soatdev/addresses` to `composer.json`.

    "soatdev/addresses": "dev-master"
    
Run `composer update` to pull down the latest version.

Edit `app/config/app.php` and add the `provider` and `facade`

    'providers' => [
        //Collective
        Collective\Html\HtmlServiceProvider::class,
        //Soatdev
        \Soatdev\Addresses\AddressesServiceProvider::class,
    ]

Now add the alias.

    'aliases' => [
        //Collective
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,

        //Soatdev
        'Addresses' => Soatdev\Addresses\Facades\Addresses::class,
    ]
    

## Initialization

You can start by publishing the configuration. This is a required step, it contains the migrations, sseds, views,  controllers and traits necessary to use the package:

    $ php artisan vendor:publish

Next migrate the database:

    $ php artisan migrate
    
To make sure the data is seeded insert the following code in the `seeds/DatabaseSeeder.php`

    //Seed the countries
    $this->call(SeedCountriesTable::class);
    //Seed the street types
    $this->call(SeedStreetTypesTable::class);

Then run 
    composer dump-autoload

And seed the database
    
    $ php artisan db:seed

For the regions and cities, you can use the sql files, I will add seeder down the road.
TO DO : Create seeders for regions and cities

## Usage

You may now add the `Geolocalizable trait` to any Class you which to add addresses to:

    use \Soatdev\Addresses\Traits\Geolocalizable;

And use the Form macro to display the address form fields. You need to wrap this macro in a form which does whatever action you want it to:

    {!! Form::sd_address_fields($errors) !!}

Then, in the controller that handles the form action, you can call the static store method on Address with the request and the object you the address to be bound to (That object needs to use the Geolocalizable Trait:

        //Create address for a geolocalizable entity
        $result = Address::store($request, $entity);
        if(!($result instanceOf Address)){
            //If the store method fails, it returns the validator tha can then be sent back with a redirect.
            return back()
            ->withErrors($result)
            ->withInput();
        }
        //If the store method succeeds, it return the Address instance created for the entity.
        return $entity->addresses();


## Configuration

TO DO

##Tests
When publishing this package, it copies all the necessary factories to run the tests:
    vendor/bin/phpunit vendor/soatdev/addresses/tests


##Thanks

Special thanks to Christoph Kempen and Paul Kievits for their laravel-countries package and to Rohit Kumar for his database integration of countries, regions and cities : hiiamrohit/Countries-States-Cities-database
