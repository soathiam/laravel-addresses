<?php

Route::group(['namespace' => 'Soatdev\Addresses\Controllers', 'prefix'=>'addresses'], function() {

	Route::resource('address', 'AddressPublicController');
	

	Route::group(['prefix'=>'api'], function() {

		
		//Get all the countries
		Route::any('streettypes/dropdown', function(){
			$streettypes = \Soatdev\Addresses\Models\CtrlStreetType::forDropdown();
			return \Response::make($streettypes);
		});

		//Get all the countries
		Route::any('countries/dropdown', function(){
			$countries = \Soatdev\Addresses\Models\CtrlCountry::pluck('name', 'id');
			return \Response::make($countries);
		});

		//Get all the regions for a country
		Route::any('countries/{country_id}/regions/dropdown', function($country_id){
			$regions = \Soatdev\Addresses\Models\CtrlRegion::byCountryForDropdown($country_id);
			return \Response::make($regions);
		});

		//Get all the cities for a country and region
		Route::any('countries/{country_id}/regions/{region_id}/cities/dropdown', function($country_id, $region_id){
			$cities = \Soatdev\Addresses\Models\CtrlCity::byCountryAndRegionForDropdown($country_id, $region_id);
			return \Response::make($cities);
		});

		//Get all the cities for a country
		Route::any('countries/{country_id}/cities/dropdown', function($country_id){
			$cities = \Soatdev\Addresses\Models\CtrlCity::byCountryForDropdown($country_id);
			return \Response::make($cities);
		});

		//Get all the cities for a region
		Route::any('regions/{region_id}/cities/dropdown', function($region_id){
			$cities = \Soatdev\Addresses\Models\CtrlCity::byRegionForDropdown($region_id);
			return \Response::make($cities);
		});

		//Get all neighborhoods for a city
		Route::any('cities/{city_id}/neighborhoods/dropdown', function($city_id){
			$neighborhoods = \Soatdev\Addresses\Models\CtrlNeighborhood::byCityForDropdown($city_id);
			return \Response::make($neighborhoods);
		});

		//TEMP: all in json
		Route::get('/countries/json', function(){
			$d = \Soatdev\Addresses\Models\CtrlCountry::all();
			return \Response::make($d->toJson());
		});
		Route::get('/regions/json', function(){
			$d = \Soatdev\Addresses\Models\CtrlRegion::all();
			return \Response::make($d->toJson());
		});
		Route::get('/cities/json', function(){
			$d = \Soatdev\Addresses\Models\CtrlCity::all();
			return \Response::make($d->toJson());
		});
		Route::get('/neighborhoods/json', function(){
			$d = \Soatdev\Addresses\Models\CtrlNeighborhood::all();
			return \Response::make($d->toJson());
		});

	});


});

