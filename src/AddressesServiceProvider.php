<?php

namespace Soatdev\Addresses;

use Illuminate\Support\ServiceProvider;

class AddressesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Route
        include __DIR__.'/routes.php';

        // Language
        $this->loadTranslationsFrom( __DIR__.'/Lang', 'addresses');

        ////Publish
        //Config
        $this->publishes([
            __DIR__.'/Config/addresses.php' => config_path('addresses.php'),
        ], 'config');

        //Views
        //$this->publishes([
        //    __DIR__.'/Views' => base_path('resources/views/soatdev/addresses'),
        //], 'views');

        //Factories
        $this->publishes([
            __DIR__.'/Database/Factories' => $this->app->databasePath() . '/factories',
        ], 'factories');

        //Migrations
        $this->publishes([
            __DIR__ . '/Database/Migrations' => $this->app->databasePath() . '/migrations',
        ], 'migrations');

        //Seeders
        $this->publishes([
            __DIR__ . '/Database/Seeds' => $this->app->databasePath() . '/seeds',
        ], 'seeds');

        //Public folder
        $this->publishes([
            __DIR__ . '/Public' => base_path('public/addresses'),
        ], 'seeds');

        //Macros
        \Form::macro('sd_bootstrap_tb', function ($name, $label, $placeholder='', $required=false, $errors = null) {

            $errorClass = '';
            $errorMessage = '';
            if(!is_null($errors) && $errors->has($name)){
                $errorClass = 'has-error';
                $errorMessage = '<div class="col-md-12 alert-danger">'.$errors->first($name).'</div>';
            }

            $requiredSpan = '';
            if($required){
                $requiredSpan = '<span style="color:red;">*</span>';
            }
            $html = '
                <div class="form-group '.$errorClass.'">
                    <label for="'.$name.'">'.$label.'</label>'.$requiredSpan.'
                    <input class="form-control" placeholder="'.$placeholder.'" autofocus="autofocus" name="'.$name.'" type="text" value="'. \Request::old($name) .'">                    
                </div>
            ';

            $html .= $errorMessage;
            return $html;
        });

        \Form::macro('sd_bootstrap_dropdown', function ($name, $label, $placeholder, $list=null, $required=false, $class='', $errors = null) {
            $errorClass = '';
            $errorMessage = '';
            if(!is_null($errors) && $errors->has($name)){
                $errorClass = 'has-error';
                $errorMessage = '<div class="col-md-12 alert-danger">'.$errors->first($name).'</div>';
            }

            $requiredSpan = '';
            if($required){
                $requiredSpan = '<span style="color:red;">*</span>';
            }

            $options = '';
            if(!is_null($list)){
                foreach ($list as $key => $value) {
                    $options .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }

            $html = '
                <div class="form-group '.$errorClass.'">
                    <label for="'.$name.'">'.$label.' '.$requiredSpan.'</label>

                    <select class="form-control '.$class.'" name="'.$name.'" id="'.$name.'">
                    <option value="">'.$placeholder.'</option>
                    '.$options.'
                    </select>
                </div>
            ';
            $html .= $errorMessage;
            return $html;
        });

        \Form::macro('sd_cities_dropdowns', function ($errors=null) {

            $html ='';
                $html .= '<div  class="col-md-6">';
                    $html .= \Form::sd_bootstrap_dropdown('country_id', '', 'Select Country', null, false, 'countries', $errors);
                $html .='</div>';

                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_dropdown('region_id', '', 'Select Region', null, false, 'states', $errors);
                $html .='</div>';

                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_dropdown('city_id', '', 'Select City', null, false, 'cities', $errors);
                $html .='</div>';
            
            $html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="'.asset('addresses/js/addresses.js').'"></script>';

            return $html;
        });

        \Form::macro('sd_address_fields', function ($errors=null, $includeBootstrap = false) {
            
            $html ='';

            /*
            if($errors->messages()){
                $html .= '<div  class="col-md-12">';
                $html .= '<ul class="list-group">';
                foreach ($errors->messages() as $key => $value) {
                    $html .='<li class="list-group-item alert-danger">'.$value[0].'</li>';
                }
                $html .='</ul>';
                $html .='</div>';
      
            }   
            */

            $html .='<div>';
                
                $html .= '<div  class="col-md-12">';
                    $html .= \Form::sd_bootstrap_tb('name', '', trans('address_name'), false, $errors);
                $html .='</div>';

                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_tb('civic_number_1', '', trans('civic_number'), false, $errors);
                $html .='</div>';
                
                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_dropdown('street_type_id', '', trans('street_type'), null, false, 'streettypes', $errors);
                $html .='</div>';
                
                $html .= '<div  class="col-md-6">';
                    $html .= \Form::sd_bootstrap_tb('street_name', '', trans('street_name'), false, $errors);
                $html .='</div>';
                
                $html .= '<div  class="col-md-4">';
                    $html .= \Form::sd_bootstrap_tb('near', '', trans('near'), false, $errors);                     
                $html .='</div>';
                
                $html .= '<div  class="col-md-2">';
                    $html .= \Form::sd_bootstrap_tb('appartment_number', '', trans('apt_number'), false, $errors);
                $html .='</div>';
                
                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_dropdown('appartment_type_id', '', trans('apt_type'), null, false, '', $errors);
                $html .='</div>';
                
                $html .= '<div  class="col-md-3">';
                    $html .= \Form::sd_bootstrap_tb('postal_code', '', trans('postal_code'), false, $errors);
                $html .='</div>';

                $html .= \Form::sd_cities_dropdowns($errors);


            $html .='</div>';


            if($includeBootstrap){
                $html .='
                <!-- Latest compiled and minified CSS -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

                <!-- Optional theme -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

                <!-- Latest compiled and minified JavaScript -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                ';
            }
            
            return $html;
        });

        \Form::macro('sd_address_form', function ($formAction='#', $formMethod='POST') {

            $html ='<form action="'.$formAction.'">';

            $html .= \Form::sd_address_fields();

            $html .='<button type="submit" class="btn btn-primary">'.trans('save').'</button>';

            $html .= '</form>';
            
            return $html;
        });


                    
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Config
        $this->mergeConfigFrom( __DIR__.'/Config/addresses.php', 'addresses');

        $this->app['addresses'] = $this->app->share(function($app) {
            return new Addresses;
        });
    }
}
