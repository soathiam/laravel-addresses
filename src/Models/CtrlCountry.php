<?php

namespace Soatdev\Addresses\Models;

class CtrlCountry extends \Soatdev\Addresses\Models\CtrlTable
{
	public static function regionsForDropdown($country_id)
	{
		return CtrlRegion::byCountryForDropdown($country_id);
	}

	/**
	* Fetches a list of cities for a specified country and optionnal region
	* Return a dropdown compatible lists
	* 
	*/
	public static function citiesForDropdown($country_id, $region_id = null)
	{	

		//When region_id is null, we want to retreive all the cities for this country
		if(is_null($region_id)){
			
			return CtrlCity::byCountryForDropdown($country_id);
		}

		//Otherwise, we fetch the cities for the combination country/region; this way, if a request is sent for a region that does not belong to to the specified country, an error message will be sent back.

		return CtrlCity::byCountryAndRegionForDropdown($country_id, $region_id);

	}

	public function regions()
	{
		return $this->hasMany('\Soatdev\Addresses\Models\CtrlRegion', 'country_id');
	}
	public function cities()
	{
		return $this->hasMany('\Soatdev\Addresses\Models\CtrlCity', 'country_id');
	}


}