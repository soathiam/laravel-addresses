<?php

namespace Soatdev\Addresses\Models;

class CtrlCity extends \Soatdev\Addresses\Models\CtrlTable
{
	public static function byCountry($country_id)
	{
		return CtrlCity::where('country_id', '=', $country_id)->get();
	}

	public static function byCountryForDropdown($country_id)
	{
		return CtrlCity::byCountry($country_id)->pluck('name', 'id');
	}

	public static function byRegion($region_id)
	{
		return CtrlCity::where('region_id', '=', $region_id)->get();
	}

	public static function byRegionForDropdown($region_id)
	{
		return CtrlCity::byRegion($region_id)->pluck('name', 'id');
	}

	public static function byCountryAndRegion($country_id, $region_id)
	{	
		return CtrlRegion::leftJoin('ctrl_cities', 'ctrl_regions.id', '=', 'ctrl_cities.region_id')
			->where('ctrl_regions.country_id', '=', $country_id)
			->where('ctrl_regions.id', '=', $region_id)
			->get();
	}

	public static function byCountryAndRegionForDropdown($country_id, $region_id)
	{
		return CtrlCity::byCountryAndRegion($country_id, $region_id)->pluck('name', 'id');
	}

	public static function neighborhoodsForDropdown($city_id)
	{
		return \Soatdev\Addresses\Models\CtrlNeighborhood::byCityForDropdown($city_id);
	}


	public function neighborhoods()
	{
		return $this->hasMany('\Soatdev\Addresses\Models\CtrlNeighborhood', 'city_id');
	}
}