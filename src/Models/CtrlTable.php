<?php

namespace Soatdev\Addresses\Models;

use \Illuminate\Database\Eloquent\Model;

class CtrlTable extends Model
{
	protected $fillable = ['code', 'name'];

	public static function forDropdown()
	{
		return static::pluck('name', 'id');
	}

	
}