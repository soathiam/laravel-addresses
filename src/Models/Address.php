<?php

namespace Soatdev\Addresses\Models;

use \Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $fillable = ['owner_class', 'owner_id_attribute', 'owner_id_value', 'name', 'neighborhood_id', 'street_type_id', 'appartment_type_id', 'civic_number_1', 'civic_number_2', 'street_name', 'appartment_number', 'postal_code', 'near', 'country_id', 'region_id', 'city_id'];


	public static function allForEntity($entity)
	{
		$id_attribute = $entity->identifying_attribrute;
		return Address::where('owner_class', '=', get_class ($entity))
			->where('owner_id_attribute', '=', $id_attribute)
			->where('owner_id_value', '=', $entity->$id_attribute)
			->get();
	}

	public static function fromNameForEntity($name, $entity)
	{
		$id_attribute = $entity->identifying_attribrute;
		return Address::where('owner_class', '=', get_class ($entity))
			->where('owner_id_attribute', '=', $id_attribute)
			->where('owner_id_value', '=', $entity->$id_attribute)
			->where('name', '=', $name)
			->first();
	}

	public static function add($entity, $address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, \Soatdev\Addresses\Models\CtrlCity $city, $appartment_type_id =null, $appartment_number=null, $civic_number_2=null, $near=null, $neighborhood_id=null, $longitude=null, $latitude=null)
	{
		$existingAddress = Address::fromNameForEntity($address_name, $entity);
		if($existingAddress){
			throw new \Exception("An address with this name already exists.", 1);
			
		}

		$id_attribute = $entity->identifying_attribrute;
		$address = new Address(
			[
				'owner_class'=>get_class($entity),
				'owner_id_attribute'=>$id_attribute,
				'owner_id_value'=>$entity->$id_attribute,
				'name'=>$address_name,
				'civic_number_1'=>$civic_number_1,
				'civic_number_2'=>$civic_number_2,
				'street_name'=>$street_name,
				'street_type_id'=>$street_type_id,
				'appartment_type_id'=>$appartment_type_id,
				'appartment_number'=>$appartment_number,
				'postal_code'=>str_replace(' ', '', $postal_code) ,
				'city_id'=>$city->id,
				'country_id'=>$city->country_id,
				'region_id'=>$city->region_id,
				'neighborhood_id'=>$neighborhood_id,
				'near'=>$near,
				'longitude'=>$longitude,
				'latitude'=>$latitude,
			]
		);

		return $address->save();
	}

	public static function addAddressToEntity($address, $entity)
	{
		$id_attribute = $entity->identifying_attribrute;
		$address->owner_class = get_class($entity);
		$address->owner_id_attribute = $id_attribute;
		$address->owner_id_value = $entity->$id_attribute;
		return $address->save();

	}

	public static function store($request, $entity)
    {
    	$rules = [
            'name' => 'required|max:255',
            'civic_number_1' => 'required',
            'street_type_id' => 'required',
            'street_name' => 'required|max:255',
            //'appartment_type_id' => 'required',
            //'appartment_number' => 'required',
            'postal_code' => 'required|max:15',
            'country_id' => 'required',
            'region_id' => 'required',
            'city_id' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();
            return $validator;

        } 
        //To avoid integrity constraint on foreign key
        if($request->input('appartment_type_id') == ''){
        	$request->merge(array('appartment_type_id' => null));
        }

        //if entity is provided, add the values to the request
        if(!is_null($entity)){
        	
        	$entity_values=array(
        		'owner_class' => get_class($entity),
        		'owner_id_attribute' => $entity->identifying_attribrute,
        		'owner_id_value' => $entity->getIdentifyingAttribruteValue(),

        	);
        	$request->merge($entity_values);
        }

        $address = new \Soatdev\Addresses\Models\Address($request->all());
        $address->save();
        return $address;
    }

	public function __toString()
	{
		$s = $this->civic_number_1;
		$s .= $this->civic_number_2?' '.$this->civic_number_2:'';
		$s .= $this->street_type_id?' '.CtrlStreetType::find($this->street_type_id)->name:'';
		$s .= ' '.$this->street_name;
		$s .= ($this->appartment_type_id && $this->appartment_number)?', '.CtrlAppartmentType::find($this->appartment_type_id)->name:'';
		$s .= $this->appartment_number?' '.$this->appartment_number:'';
		$s .= $this->near?' (near '.$this->near.')':'';
		$s .= ', '.$this->postal_code;
		$s .= ' '.CtrlCity::find($this->city_id)->name;
		$s .= ', '.CtrlCountry::find($this->country_id)->name;
		$s .= $this->region_id?' ('.CtrlRegion::find($this->region_id)->name.')':'';
		return $s;
	}


	public function setOwner($entity)
	{
		$id_attribute = $entity->identifying_attribrute;
		$this->owner_class = get_class($entity);
		$this->owner_id_attribute = $id_attribute;
		$this->owner_id_value = $entity->$id_attribute;
		return $this->save();
	}

	public function country()
	{
		return $this->belongsTo('\Soatdev\Addresses\Models\CtrlCountry', 'country_id');
	}
}