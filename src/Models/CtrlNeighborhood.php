<?php

namespace Soatdev\Addresses\Models;

class CtrlNeighborhood extends \Soatdev\Addresses\Models\CtrlTable
{
	public static function byCity($city_id)
	{
		return CtrlNeighborhood::where('city_id', '=', $city_id)->get();
	}

	public static function byCityForDropdown($city_id)
	{
		return CtrlNeighborhood::byCity($city_id)->pluck('name', 'id');
	}
}