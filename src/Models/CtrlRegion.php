<?php

namespace Soatdev\Addresses\Models;

class CtrlRegion extends \Soatdev\Addresses\Models\CtrlTable
{
	public static function byCountry($country_id)
	{
		return CtrlRegion::where('country_id', '=', $country_id)->get();
	}

	public static function byCountryForDropdown($country_id)
	{
		return CtrlRegion::byCountry($country_id)->pluck('name', 'id');
	}

	public function cities()
	{
		return $this->hasMany('\Soatdev\Addresses\Models\CtrlCity', 'region_id');
	}
}