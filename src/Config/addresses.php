<?php
return [
	'default_identifying_attribrute' => 'id',

	
	'css' => [
	
		'row_class'					=>'',
		'full_div_class'			=>'',// 1/1
		'three_fourth_div_class'	=>'',// 3/4
		'two_third_div_class'		=>'',// 2/3
		'half_div_class'			=>'',// 1/2
		'third_div_class'			=>'',// 1/3
		'fourth_div_class'			=>'',// 1/4
		'fith_div_class'			=>'',// 1/5
		'sixth_div_class'			=>'',// 1/6

		
		'field_div_css_error_class'	=> 'has-error',

		'label_div_css_class'		=> '',
		'required_field_span_class' => '',

		'input_div_css_class'		=> '',
		'input_dropdown_css_class'	=> 'form-control',
		
		'errors_div_css_class'		=> '',
	],
];