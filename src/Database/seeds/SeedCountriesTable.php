<?php

use Illuminate\Database\Seeder;

class SeedCountriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty the countries table
        DB::table('ctrl_countries')->delete();

        $now = \Carbon\Carbon::now();
        //Get all of the countries
        $countries = $this->countries = json_decode(file_get_contents(__DIR__ . '/countries.json'), true);
        
        foreach ($countries as $countryId => $country){
            DB::table('ctrl_countries')->insert(array(
                'id' => $countryId,
                'capital' => ((isset($country['capital'])) ? $country['capital'] : null),
                'citizenship' => ((isset($country['citizenship'])) ? $country['citizenship'] : null),
                'code' => $country['iso_3166_3'],
                'currency' => ((isset($country['currency'])) ? $country['currency'] : null),
                'currency_code' => ((isset($country['currency_code'])) ? $country['currency_code'] : null),

                'full_name' => ((isset($country['full_name'])) ? $country['full_name'] : null),
                'iso_3166_2' => $country['iso_3166_2'],
                'iso_3166_3' => $country['iso_3166_3'],
                'name' => $country['name'],

                'calling_code' => $country['calling_code'],
                'currency_symbol' => ((isset($country['currency_symbol'])) ? $country['currency_symbol'] : null),
                'flag' =>((isset($country['flag'])) ? $country['flag'] : null),
                'created_at'=>$now,
                'updated_at'=>$now,
            ));
        }
    }
}
