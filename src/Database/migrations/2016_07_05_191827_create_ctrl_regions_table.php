<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlRegionsTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_regions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('country_id')->unsigned();
			$table->string('code')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::table('ctrl_regions', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('ctrl_countries')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('ctrl_regions');
	}
}