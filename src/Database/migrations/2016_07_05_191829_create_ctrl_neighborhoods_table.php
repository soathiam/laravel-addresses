<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlNeighborhoodsTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_neighborhoods', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('city_id')->unsigned();
			$table->string('code')->unique();
			$table->string('name');
			$table->timestamps();
			
		});

		Schema::table('ctrl_neighborhoods', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('ctrl_cities')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('ctrl_neighborhoods');
	}
}