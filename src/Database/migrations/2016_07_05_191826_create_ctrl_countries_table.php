<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlCountriesTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_countries', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code')->unique();
			$table->string('name');
			$table->string('full_name')->nullable();

			$table->string('capital')->nullable();
			$table->string('citizenship')->nullable();
			
			$table->string('iso_3166_2');
			$table->string('iso_3166_3');

			$table->string('currency')->nullable();
			$table->string('currency_code')->nullable();
			$table->string('currency_symbol')->nullable();


			$table->integer('calling_code')->nullable();
			$table->string('flag')->nullable();
			$table->timestamps();

		});
	}

	public function down()
	{
		Schema::drop('ctrl_countries');
	}
}