<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlStreetTypesTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_street_types', function(Blueprint $table) {
			$table->increments('id');			
			$table->string('code')->unique();
			$table->string('name');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('ctrl_street_types');
	}
}