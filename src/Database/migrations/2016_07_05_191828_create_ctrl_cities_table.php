<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlCitiesTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_cities', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('country_id')->unsigned();
			$table->integer('region_id')->unsigned()->nullable();
			$table->string('code')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::table('ctrl_cities', function(Blueprint $table) {
			$table->foreign('region_id')->references('id')->on('ctrl_regions')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('ctrl_cities', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('ctrl_countries')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('ctrl_cities');
	}
}