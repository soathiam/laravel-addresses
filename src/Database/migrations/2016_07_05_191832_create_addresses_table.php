<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration {

	public function up()
	{
		Schema::create('addresses', function(Blueprint $table) {
			$table->increments('id');
			
			$table->string('name')->default('Default');
			
			$table->integer('country_id')->unsigned();
			$table->integer('region_id')->unsigned()->nullable();
			$table->integer('city_id')->unsigned();
			$table->integer('neighborhood_id')->unsigned()->nullable();
			$table->integer('street_type_id')->unsigned();
			$table->integer('appartment_type_id')->unsigned()->nullable();

			$table->string('owner_class');
			$table->string('owner_id_attribute');
			$table->string('owner_id_value');
			
			$table->string('civic_number_1', 45);
			$table->string('civic_number_2', 45)->nullable();
			$table->string('street_name');
			
			
			$table->string('appartment_number', 45)->nullable();
			$table->string('near')->nullable();
			$table->string('postal_code', 10);
			$table->string('latitude', 45)->nullable();
			$table->string('longitude', 45)->nullable();

			$table->timestamps();
			$table->integer('created_by_user_id')->unsigned()->nullable();
			$table->integer('updated_by_user_id')->unsigned()->nullable();

			//$table->unique(['name','owner_class','owner_id_value']);
		});


		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('ctrl_countries')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('region_id')->references('id')->on('ctrl_regions')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('ctrl_cities')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('neighborhood_id')->references('id')->on('ctrl_neighborhoods')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('street_type_id')->references('id')->on('ctrl_street_types')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('addresses', function(Blueprint $table) {
			$table->foreign('appartment_type_id')->references('id')->on('ctrl_appartment_types')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('addresses');
	}
}