<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(\Soatdev\Addresses\Models\CtrlCountry::class, function (Faker\Generator $faker) {
    return [        
        'name' => $faker->country,
        'code' => $faker->slug
    ];
});

$factory->define(\Soatdev\Addresses\Models\CtrlRegion::class, function (Faker\Generator $faker) {
    $country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
    return [        
        'name' => $faker->city,
        'code' => $faker->slug,
        'country_id' => $country->id,
    ];
});

$factory->define(\Soatdev\Addresses\Models\CtrlCity::class, function (Faker\Generator $faker) {
    $region = factory(\Soatdev\Addresses\Models\CtrlRegion::class)->create();
    return [        
        'name' => $faker->city,
        'code' => $faker->slug,
        'country_id' => $region->country_id,
        'region_id' => $region->id,
    ];
});

$factory->define(\Soatdev\Addresses\Models\CtrlNeighborhood::class, function (Faker\Generator $faker) {
    $city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
    return [        
        'name' => $faker->word,
        'code' => $faker->slug,
        'city_id' => $city->id,
    ];
});

$factory->define(\Soatdev\Addresses\Models\CtrlStreetType::class, function (Faker\Generator $faker) {
    return [        
        'name' => 'Avenue',
        'code' => $faker->slug,
    ];
});

$factory->define(\Soatdev\Addresses\Models\CtrlAppartmentType::class, function (Faker\Generator $faker) {
    return [        
        'name' => 'Suite',
        'code' => $faker->slug,
    ];
});


$factory->define(\Soatdev\Addresses\Models\Address::class, function (Faker\Generator $faker) {
    $city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
    $street_type = factory(\Soatdev\Addresses\Models\CtrlStreetType::class)->create();
    $appartment_type = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class)->create();
    return [        
        'name' => $faker->unique()->word,
        'civic_number_1' => $faker->randomDigit,
        'civic_number_2' => 'bis',
        'street_name' => $faker->word,
        'appartment_number' => $faker->randomDigit,
        'postal_code' => $faker->postcode,
        'street_name' => $faker->word,
        'near' => $faker->sentence,
        'country_id' => $city->country_id,
        'city_id' =>$city->id,
        'street_type_id' =>$street_type->id,
        'appartment_type_id' =>$appartment_type->id,

    ];
});