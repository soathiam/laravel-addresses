<?php 
namespace Soatdev\Addresses\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class Addresses extends Facade {
 
    protected static function getFacadeAccessor() { return 'addresses'; }
 
}