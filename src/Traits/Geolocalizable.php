<?php
namespace Soatdev\Addresses\Traits;

trait Geolocalizable {

	public $identifying_attribrute = 'id';//config('addresses.default_identifying_attribrute');//Overridable
	public function addresses()
	{
		$id_attribute = $this->identifying_attribrute;
		//return $this->hasMany('\Soatdev\Addresses\Models\Address', 'owner_id_value', $this->identifying_attribrute);
		return \Soatdev\Addresses\Models\Address::where('owner_class', '=', get_class($this))
			->where('owner_id_value', '=', $this->$id_attribute)
			->get();
	}
	public function addNewAddress($address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, \Soatdev\Addresses\Models\CtrlCity $city, $appartment_type_id =null, $appartment_number=null, $civic_number_2=null, $near=null, $neighborhood_id=null)
	{

		return \Soatdev\Addresses\Models\Address::add($this, $address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, $city, $appartment_type_id, $appartment_number, $civic_number_2, $near, $neighborhood_id);
	}

	public function addAddress($address)
	{
		return \Soatdev\Addresses\Models\Address::addAddressToEntity($address, $this);
	}

	public function getAddress($address_name)
	{
		return $this->addresses()->where('name', $address_name)->first();
	}

	public function getIdentifyingAttribruteValue()
	{
		$id_attribute = $this->identifying_attribrute;
		return $this->$id_attribute;
	}
}