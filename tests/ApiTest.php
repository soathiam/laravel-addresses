<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
class ApiTest extends TestCase
{
	use DatabaseTransactions;
	
	public function setUp()
	{
		parent::setUp();
		
	}

	/** @test */
	function it_shows_the_list_of_street_types_when_visiting_the_url()
	{
		$countries = \Soatdev\addresses\Models\CtrlStreetType::forDropdown();
		$this->visit('addresses/api/streettypes/dropdown')
		->see($countries);
	}

	/** @test */
	function it_shows_the_list_of_countries_when_visiting_the_url()
	{
		$countries = \Soatdev\addresses\Models\CtrlCountry::forDropdown();
		$this->visit('addresses/api/countries/dropdown')
		->see($countries);
	}
	
	/** @test */
	function it_sees_a_list_of_regions_by_country_when_visiting_the_api()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$regions = \Soatdev\Addresses\Models\CtrlRegion::byCountry($country->id);

		$this->visit('addresses/api/countries/'.$country->id.'/regions/dropdown')
		->see($regions->lists('name', 'id'));
	}

	/** @test */
	function it_sees_a_list_of_cities_by_country_when_visiting_the_api()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$cities = \Soatdev\Addresses\Models\CtrlCity::byCountry($country->id);

		$this->visit('addresses/api/countries/'.$country->id.'/cities/dropdown')
		->see($cities->lists('name', 'id'));
	}

	/** @test */
	function it_sees_a_list_of_cities_by_region_and_country_when_visiting_the_api()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 2)->create(['country_id'=>$country->id]);
		
		$cities0 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[0]->id]);
		
		$cities1 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[1]->id]);

		$this->visit('addresses/api/countries/'.$country->id.'/regions/'.$regions[0]->id.'/cities/dropdown')
		->see($cities0->lists('name', 'id'));

		$this->visit('addresses/api/countries/'.$country->id.'/regions/'.$regions[1]->id.'/cities/dropdown')
		->see($cities1->lists('name', 'id'));


	}

	/** @test */
	function it_sees_a_list_of_neighborhoods_by_city_when_visiting_the_api()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create(['country_id'=>$country->id]);

		$this->visit('addresses/api/cities/'.$city->id.'/neighborhoods/dropdown')
		->see($city->neighborhoods()->lists('name', 'id'));

	}


}