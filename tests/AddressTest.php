<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\Model;

use \Soatdev\Addresses\Models\Address;


class AddressTest extends \TestCase
{
	use DatabaseTransactions;
	
	protected $address;
	public function setUp()
	{
	    parent::setUp();
	    
	    $this->address = factory(Address::class)->create();
		
	}

	/** @test */
	function it_can_add_a_new_address_to_an_entity()
	{
		$object = new AddressTestObject(1, 'testObject');
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
	    $street_type = factory(\Soatdev\Addresses\Models\CtrlStreetType::class)->create();
	    $appartment_type = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class)->create();
		//$entity, $address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, $city, $appartment_type_id =null, $appartment_number=null, $civic_number_2=null, $near=null
		Address::add($object, 'Home', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);

		$address = Address::fromNameForEntity('Home', $object);
		$this->assertEquals('H3C0P3', $address->postal_code);

	}

	/** @test */
	function it_throws_an_exception_when_adding_an_address_name_that_already_exists()
	{
		$object = new AddressTestObject(1, 'testObject');
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
	    $street_type = factory(\Soatdev\Addresses\Models\CtrlStreetType::class)->create();
	    $appartment_type = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class)->create();
		//$entity, $address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, $city, $appartment_type_id =null, $appartment_number=null, $civic_number_2=null, $near=null
		Address::add($object, 'Home', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);
		$this->setExpectedException('Exception');
		Address::add($object, 'Home', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);
	}

	/** @test */
	function it_can_add_an_existing_address_to_an_entity()
	{
		$object = new AddressTestObject(1, 'testObject');
		$this->assertNotEquals($object->id, $this->address->owner_id_value);
		Address::addAddressToEntity($this->address, $object);
		$this->assertEquals($object->id, $this->address->owner_id_value);
	}

	/** @test */
	function it_can_add_multiple_existing_addresses_to_an_entity()
	{
		$object = new AddressTestObject(1, 'testObject');
		$addresses = factory(\Soatdev\Addresses\Models\Address::class, 5)->create();

		$this->assertNotEquals($object->id, $this->address->owner_id_value);
		Address::addAddressToEntity($this->address, $object);
		$this->assertEquals($object->id, $this->address->owner_id_value);
	}

	/** @test */
	function it_can_find_all_addresses_for_an_entity()
	{
		$object = new AddressTestObject(2, 'testObject');

		$id_attribute = $object->identifying_attribrute;
		$addresses = factory(\Soatdev\Addresses\Models\Address::class, 5)->create([
			'owner_class' => get_class($object),
			'owner_id_attribute' => 'id',
			'owner_id_value' => $object->id,
			]);
		$this->assertCount(5, Address::allForEntity($object));

	}

	/** @test */
	function it_can_retreive_its_country()
	{
		$this->assertTrue($this->address->country instanceof \Soatdev\Addresses\Models\CtrlCountry);
	}

	/** @test */
	function it_can_change_its_owner()
	{
		$object = new AddressTestObject(1, 'testObject');
		$object->identifying_attribrute = 'code';
		$this->assertNotEquals($object->code, $this->address->owner_id_value);
		$this->address->setOwner($object);
		$this->assertEquals($object->code, $this->address->owner_id_value);
	}

	/** @test */
	function it_can_display_itself_nicely()
	{
		$this->assertContains($this->address->street_name, (string)$this->address);	
	}

	/** @test */
	function its_owner_can_retreive_all_its_addresses()
	{
		$object = new AddressTestObject(2, 'testObject');

		$id_attribute = $object->identifying_attribrute;
		$addresses = factory(\Soatdev\Addresses\Models\Address::class, 5)->create([
			'owner_class' => get_class($object),
			'owner_id_attribute' => 'id',
			'owner_id_value' => $object->id,
			]);
		$this->assertCount(5, $object->addresses());
	}

	/** @test */
	function its_owner_can_retreive_an_address_from_name()
	{
		$object = new AddressTestObject(3, 'testObject');
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
	    $street_type = factory(\Soatdev\Addresses\Models\CtrlStreetType::class)->create();
	    $appartment_type = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class)->create();
	    $neighborhood = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class)->create();
	    $object->addNewAddress('Home', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);
		$this->assertCount(1, $object->addresses());
		$this->assertEquals('Ottawa', $object->getAddress('Home')->street_name);
	}

	/** @test */
	function its_owner_can_add_itself_a_new_address()
	{
		$object = new AddressTestObject(4, 'testObject');
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create();
	    $street_type = factory(\Soatdev\Addresses\Models\CtrlStreetType::class)->create();
	    $appartment_type = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class)->create();
	    $neighborhood = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class)->create();

	    //($address_name, $civic_number_1, $street_name, $street_type_id, $postal_code, CtrlCity $city, $appartment_type_id =null, $appartment_number=null, $civic_number_2=null, $near=null, $neighborhood_id=null)

		$object->addNewAddress('Home', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);
		$this->assertCount(1, $object->addresses());

		$object->addNewAddress('Office', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city, $appartment_type->id, 431, null, 'New City Gas Night Club');
		
		$this->assertCount(2, $object->addresses());

		$this->assertEquals('New City Gas Night Club', $object->getAddress('Office')->near);
		$this->setExpectedException('Exception');
		$object->addNewAddress('Office', 1000, 'Ottawa', $street_type->id, 'H3C 0P3', $city);
	}

}

class AddressTestObject{
	use \Soatdev\Addresses\Traits\Geolocalizable;
	public $id;
	public $code;

	public function __construct($id, $code)
	{
		$this->id = $id;
		$this->code = $code;
	}
}