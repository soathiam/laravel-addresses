<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Soatdev\Addresses\Models\CtrlCountry;
class CtrlTablesTest extends TestCase
{
	use DatabaseTransactions;
	
	public function setUp()
	{
		parent::setUp();
		
	}

	/** @test */
	function a_country_can_retrieve_all_its_regions()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 3)->create(['country_id'=>$country->id]);
		$this->assertCount(3, $country->regions);
	}

	/** @test */
	function a_country_can_retrieve_all_its_cities()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlCity::class, 3)->create(['country_id'=>$country->id]);
		$this->assertCount(3, $country->cities);
	}

	/** @test */
	function static_country_can_get_all_for_dropdown()
	{
		$countries = factory(\Soatdev\Addresses\Models\CtrlCountry::class, 10)->create();
		$id = $countries[3]->id;
		$this->assertEquals($countries[3]->name, \Soatdev\Addresses\Models\CtrlCountry::forDropdown()[$id]);
	}

	/** @test */
	function static_country_can_get_all_regions_from_a_country_id()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$regions = \Soatdev\Addresses\Models\CtrlRegion::byCountry($country->id);
		$this->assertEquals($regions->lists('name', 'id'), \Soatdev\Addresses\Models\CtrlCountry::regionsForDropdown($country->id));
	}

	/** @test */
	function static_country_can_get_all_cities_from_a_country()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$cities = \Soatdev\Addresses\Models\CtrlCity::byCountry($country->id);
		$this->assertEquals($cities->lists('name', 'id'), \Soatdev\Addresses\Models\CtrlCountry::citiesForDropdown($country->id));
	}

	/** @test */
	function static_country_can_get_all_cities_from_a_country_id_and_region_id()
	{
		$country = \Soatdev\Addresses\Models\CtrlCountry::first();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 2)->create(['country_id'=>$country->id]);
		$cities1 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[0]->id]);
		$cities2 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[1]->id]);
		
		$this->assertEquals($cities1->lists('name', 'id'), \Soatdev\Addresses\Models\CtrlCountry::citiesForDropdown($country->id, $regions[0]->id));
		$this->assertEquals($cities2->lists('name', 'id'), \Soatdev\Addresses\Models\CtrlCountry::citiesForDropdown($country->id, $regions[1]->id));
	}

	/** @test */
	function static_region_can_get_all_by_country_id()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 6)->create(['country_id'=>$country->id]);
		$this->assertCount(6, \Soatdev\Addresses\Models\CtrlRegion::byCountry($country->id));
	}

	/** @test */
	function static_region_can_get_all_by_country_id_for_dropdown()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 6)->create(['country_id'=>$country->id]);
		$id = $regions[3]->id;
		$this->assertEquals($regions[3]->name, \Soatdev\Addresses\Models\CtrlRegion::byCountryForDropdown($country->id)[$id]);
	}

	/** @test */
	function static_city_can_get_all_by_country_id_or_region_id()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 2)->create(['country_id'=>$country->id]);
		$cities1 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[0]->id]);
		$cities2 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 4)->create(['country_id'=>$country->id, 'region_id'=>$regions[1]->id]);
		$this->assertCount(10, \Soatdev\Addresses\Models\CtrlCity::byCountry($country->id));
		$this->assertCount(6, \Soatdev\Addresses\Models\CtrlCity::byRegion($regions[0]->id));
		$this->assertCount(4, \Soatdev\Addresses\Models\CtrlCity::byRegion($regions[1]->id));
	}

	/** @test */
	function static_city_can_get_all_by_country_id_or_region_id_for_dropdown()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 2)->create(['country_id'=>$country->id]);
		$cities1 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 6)->create(['country_id'=>$country->id, 'region_id'=>$regions[0]->id]);
		$cities2 = factory(\Soatdev\Addresses\Models\CtrlCity::class, 4)->create(['country_id'=>$country->id, 'region_id'=>$regions[1]->id]);

		$id = $cities1[3]->id;
		$this->assertEquals($cities1[3]->name, \Soatdev\Addresses\Models\CtrlCity::byCountryForDropdown($country->id)[$id]);
		$this->assertEquals($cities1[3]->name, \Soatdev\Addresses\Models\CtrlCity::byRegionForDropdown($regions[0]->id)[$id]);
		$id2 = $cities2[3]->id;
		$this->assertEquals($cities2[3]->name, \Soatdev\Addresses\Models\CtrlCity::byRegionForDropdown($regions[1]->id)[$id2]);
	}

	/** @test */
	function static_neighborhood_can_get_all_by_city_id()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$regions = factory(\Soatdev\Addresses\Models\CtrlRegion::class, 2)->create(['country_id'=>$country->id]);
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create(['country_id'=>$country->id, 'region_id'=>$regions[0]->id]);
		$cities = factory(\Soatdev\Addresses\Models\CtrlCity::class, 3)->create(['country_id'=>$country->id, 'region_id'=>$regions[1]->id]);
		$nei1 = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class)->create(['city_id'=>$city->id]);
		$nei2 = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class, 2)->create(['city_id'=>$cities[0]->id]);
		$nei3 = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class, 3)->create(['city_id'=>$cities[1]->id]);

		$this->assertCount(1, \Soatdev\Addresses\Models\CtrlNeighborhood::byCity($city->id));
		$this->assertCount(2, \Soatdev\Addresses\Models\CtrlNeighborhood::byCity($cities[0]->id));
		$this->assertCount(3, \Soatdev\Addresses\Models\CtrlNeighborhood::byCity($cities[1]->id));
		$this->assertCount(0, \Soatdev\Addresses\Models\CtrlNeighborhood::byCity($cities[2]->id));
	}

	/** @test */
	function static_neighborhood_can_get_all_by_city_id_for_dropdown()
	{
		$country = factory(\Soatdev\Addresses\Models\CtrlCountry::class)->create();
		$region = factory(\Soatdev\Addresses\Models\CtrlRegion::class)->create(['country_id'=>$country->id]);
		$city = factory(\Soatdev\Addresses\Models\CtrlCity::class)->create(['country_id'=>$country->id, 'region_id'=>$region->id]);
		$nei = factory(\Soatdev\Addresses\Models\CtrlNeighborhood::class, 10)->create(['city_id'=>$city->id]);
		$id = $nei[3]->id;
		$this->assertEquals($nei[3]->name, \Soatdev\Addresses\Models\CtrlNeighborhood::byCityForDropdown($city->id)[$id]);
	}

	/** @test */
	function static_street_type_can_get_all_for_dropdown()
	{
		$streettypes = \Soatdev\Addresses\Models\CtrlStreetType::all();
		$this->assertCount(count($streettypes), \Soatdev\Addresses\Models\CtrlStreetType::forDropdown());
		}

	/** @test */
	function static_appartment_type_can_get_all_for_dropdown()
	{
		$apttypes = factory(\Soatdev\Addresses\Models\CtrlAppartmentType::class, 10)->create();
		$this->assertCount(10, \Soatdev\Addresses\Models\CtrlAppartmentType::forDropdown());
		$id = $apttypes[3]->id;
		$this->assertEquals($apttypes[3]->name, \Soatdev\Addresses\Models\CtrlAppartmentType::forDropdown()[$id]);
	}
}